# CRUD-Front-Back-HTML5-Javascript-CSS3

##Projeto WEB utilizou algumas tecnologias e ferramentas facilitadoras como:
			- Webpack para unificar, minificar e obfuscar arquivos estaticos;
			- HTML5;
			- JavaScript;
			- ECMAScript 6;
			- CSS3 & SASS;
			- Nodejs;
			- Framework de view: Bootstrap;
			- Design responsivo;
			- Serviço RESTful para comunicação entre o back e o front;
			- IDE de Desenvolvimento: [VSCODE](https://code.visualstudio.com/);
      - Gerenciador de dependencias: NPM.

##Funcionalidades desenvolvidas:

	CRUD de pessoas
		Tela de pesquisa, cadastro, edição e exclusão de pessoas.
			Objeto "Pessoa" atributos: nome, data nascimento, documento de identificação, sexo, endereco.
			Validação de obrigatório para todos os campos: documento de identificação, nome, data nascimento e sexo.

##Funcionalidades não desenvolvidas:

	Relatorio de pessoas
		Tela de graficos das pessoas. 
			Grafico da faixa de idades das pessoas, faixas: ["0 a 9", "10 a 19", "20 a 29", "30 a 39", "Maior que 40"].
			Grafico contendo quantidade de pessoas que tenham sexo masculino e feminino. 

